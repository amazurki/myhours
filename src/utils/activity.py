class Activity:
    activity_name = None
    duration = None

    def __init__(self, duration, name):
        self.duration = duration
        self.activity_name = name
