class Date:
    _day_of_month = None
    _month_number = None
    _year = None

    def __init__(self, day, month, year):
        self.day_of_month = day
        self.month_number = month
        self.year = year

    def get_date(self):
        return f"{self.day_of_month} {self.month_number} {self.year}"
