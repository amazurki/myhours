import pytest

from src.utils.date import Date


@pytest.fixture
def my_date():
    return Date(10, 11, 2019)


@pytest.fixture
def other_date():
    return Date(10, 12, 2021)


def test_get_date_of_day(my_date, other_date):
    assert my_date.get_date() == "10 11 2019"
    assert other_date.get_date() == "10 12 2021"
