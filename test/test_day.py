import pytest

from src.day import Day
from src.utils.date import Date
from src.utils.activity import Activity


@pytest.fixture
def some_day():
    return Day(Date(10, 11, 2019))


@pytest.fixture
def some_activity():
    return Activity("10", "playing")


def test_add_activity_to_day(some_day, some_activity):
    some_day.add_activity(some_activity)
